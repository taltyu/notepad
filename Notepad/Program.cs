﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace Notepad
{
    class Notebook
    {
        static List<string> newNote = new List<string>();
        static List<string> shortNote = new List<string>();
        static void Main(string[] args)
        {
            
            while (true)
            {
                Console.Clear();
                Console.WriteLine("1 - Новая запись");
                Console.WriteLine("2 - Редактировать");
                Console.WriteLine("3 - Удалить");
                Console.WriteLine("4 - Просмотр");
                Console.WriteLine("5 - Просмотр всех записей");
                Console.WriteLine("6 - Выход");

                string ch = Console.ReadLine();
                if (ch == "1")
                {
                    CreateNewNote();
                }
                else if (ch == "2")
                {
                    EditNote();
                }
                else if (ch == "3")
                {
                    DeleteNote();
                }
                else if (ch == "4")
                {
                    ReadNote();
                }
                else if (ch == "5")
                {
                    ShowAllNotes();
                }
                else if (ch == "6")
                {
                    Console.Clear();
                    Console.WriteLine("До свидания!");
                    Console.ReadKey();
                    break;
                }
                else
                {
                    Console.Clear();
                    Console.WriteLine("Ошибка! Повторите ввод");
                    Console.ReadKey();
                }
                continue;
            }
            
        }
        
        static void CreateNewNote()
        {
            Console.Clear();
            Console.WriteLine("Создание новой записи:");
            Note nNote = New();
            string note = nNote.ToString();
            string sNote = nNote.ShortNote();
            newNote.Add(note);
            shortNote.Add(sNote);
        }
        static void EditNote()
        {
            Console.Clear();
            Console.WriteLine("Редактирование записей:");
            int n;
            while (true)
            {
                if (newNote.Count == 0)
                {
                    Console.WriteLine("Записей нет");
                    Console.ReadKey();
                    break;
                }
                else
                {
                    Console.Write("Номер записи: ");
                    while (!int.TryParse(Console.ReadLine(), out n))
                        Console.WriteLine("Ошибка! Вводите только цифры!");
                    if (n == 0)
                        break;
                    else if (n <= newNote.Count)
                    {
                        Note note = New();
                        newNote.Remove(newNote.ElementAt(n - 1));
                        shortNote.Remove(shortNote.ElementAt(n - 1));
                        newNote.Insert((n - 1), note.ToString());
                        shortNote.Insert((n - 1), note.ToString());
                        Console.WriteLine("Запись изменена");
                        Console.ReadKey();
                        break;
                    }
                    else
                        Console.WriteLine("Записи с таким номером нет. Повторите ввод или введите '0' чтобы выйти в меню");
                }
            }
        }
        static void DeleteNote()
        {
            Console.Clear();
            Console.WriteLine("Удаление записей:");
            int n;
            while (true)
            {
                if (newNote.Count == 0)
                {
                    Console.WriteLine("Записей нет");
                    Console.ReadKey();
                    break;
                }
                else
                {
                    Console.Write("Номер записи: ");
                    while (!int.TryParse(Console.ReadLine(), out n))
                        Console.WriteLine("Ошибка! Вводите только цифры!");
                    if (n == 0)
                        break;
                    else if (n <= newNote.Count)
                    {
                        newNote.Remove(newNote.ElementAt(n - 1));
                        shortNote.Remove(shortNote.ElementAt(n - 1));
                        Console.WriteLine("Запись удалена");
                        Console.ReadKey();
                        break;
                    }
                    else
                        Console.WriteLine("Записи с таким номером нет. Повторите ввод или введите '0' чтобы выйти в меню");
                }
            }
        }
        static void ReadNote()
        {
            Console.Clear();
            Console.WriteLine("Просмотр записей:");
            int n;
            while (true)
            {
                if (newNote.Count == 0)
                {
                    Console.WriteLine("Записей нет");
                    Console.ReadKey();
                    break;
                }
                else
                {
                    Console.Write("Номер записи: ");
                    while (!int.TryParse(Console.ReadLine(), out n))
                        Console.WriteLine("Ошибка! Вводите только цифры!");
                    if (n == 0)
                        break;
                    else if (n <= newNote.Count)
                    {
                        Console.WriteLine(newNote.ElementAt(n - 1));
                        Console.ReadKey();
                        break;
                    }
                    else
                        Console.WriteLine("Записи с таким номером нет. Повторите ввод или введите '0' чтобы выйти в меню");
                }
            }
            
            
        }
        static void ShowAllNotes()
        {
            Console.Clear();
            Console.WriteLine("Все записи:");
            if (newNote.Count == 0)
                Console.WriteLine("Записей нет");
            foreach (object o in shortNote)
            {
                Console.WriteLine(o);
            }
            Console.ReadKey();
        }
        static string IsCorrect()
        {
            string ent;
            while (true)
            {
                ent = Console.ReadLine();
                if (!string.IsNullOrEmpty(ent))
                {
                    if (char.IsUpper(ent[0]) && ent.ToCharArray().All(c => IsCyrillic(c)))
                        break;
                }
                Console.WriteLine("Неправильный формат данных, введите только буквы, начиная с заглавной:");

            }
            return ent;
        }
        static bool IsCyrillic(char c)
        {
            if (('а' <= c && c <= 'я') || ('А' <= c && c <= 'Я') || c == 'ё' || c == 'Ё')
                return true;
            return false;
        }
        static Note New()
        {
            string ent;
            Note note = new Note();
            Console.Write("Фамилия: ");
            ent = IsCorrect();
            note.surname = ent;
            Console.Write("Имя: ");
            ent = IsCorrect();
            note.name = ent;
            Console.Write("Отчество: ");
            ent = IsCorrect();
            note.fname = ent;
            Console.Write("Номер телефона: ");
            while (!long.TryParse(Console.ReadLine(), out note.numb))
                Console.WriteLine("Ошибка! Вводите только цифры!");
            Console.Write("Страна: ");
            note.country = Console.ReadLine();
            Console.Write("Дата рождения: ");
            ent = Console.ReadLine();
            if (!string.IsNullOrEmpty(ent))
            {
                while (!DateTime.TryParse(ent, out note.date))
                {
                    Console.WriteLine("Ошибка! Введите дату!");
                    ent = Console.ReadLine();
                }
            }
            Console.Write("Организация: ");
            note.org = Console.ReadLine();
            Console.Write("Должность: ");
            note.job = Console.ReadLine();
            Console.Write("Заметки: ");
            note.notes = Console.ReadLine();
            return note;
            //string sh = note.ShortNote();
            //return note.ToString();
        }
    }
}
