﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace Notepad
{
    public class Note
    {
        public string surname, name, fname, country, org, job, notes;
        public long numb;
        public DateTime date;
        public override string ToString()
        {
            return surname + " " + name + " " + fname + " " + numb + " " + country + " " + date.ToShortDateString() + " " + org + " " + job + " " + notes;
        }
        public string ShortNote()
        {
            return surname + " " + name + " " + numb;
        }

    }
}
